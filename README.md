---
author: Kiara Jouhanneau | @kajou.design | kajou.design@gmail.com | ko-fi.com/kajoudesign    
title: Mimizan  
project: paged.js sprint
book format: letter
book orientation: portrait
version: v1.0

Mimizan is known to be very touristic in the summer for its location near the Atlantic Ocean, this template is inspired by beach and summer.
---
  
## → fonts/type designers :  
### Ouroboros by [Ariel Martin Pérez & Hélène Alix Mourrier](http://velvetyne.fr/fonts/ouroboros/)  
>*The Ouroboros or uroborus is an ancient symbol depicting a serpent or dragon eating its own tail. Ouroboros is also a font for alchemists, witches, heretics, outsiders and curious people in general. It's inspired by early art-nouveau styles, which were sometimes used in relation with magical themes and symbolism, so it comes together with a set of alchemical and astronomical symbols.* 
### Degheest Types : Abordage, Equateur, Latitude by [Ange Degheest & Eugénie Bidaut](http://velvetyne.fr/fonts/degheest/)  
>*To revive means: to resurrect, to reactivate, to renew; and in many ways our work consisted in a kind of resurrection. We had indeed to reactivate the memory of Ange Degheest by diving in her archives, by exhuming the story of a woman who lived through many ages and locations. Only once we had acquired a good grasp of her life story, were we able to revive her typefaces. Reviving her designs and distributing them widely, free of charge, is our way to honour Ange Degheest’s memory and to give them a new life in the 21st century.*  
### IA Writer Mono by [Information Architects Inc.](https://github.com/iaolo/iA-Fonts)  
>*iA Writer comes bundled with iA Writer for Mac and iOS.This is a modification of IBM's Plex font. The upstream project is [here](https://github.com/IBM/plex)* 


## → the following classes :  
>> **Component Basis**  
component-body  
component-front  
component-title  
content-opener-image  
decoration  
paragraph  

>> **Component: Long**  
biography  
case-study  
feature   
long  

>> **Component: Shortboxes**  
note  
reminder  
short  
tip  
warning  

>> **Features: Closing**  
key-term  
key-terms  
references  
review-activity  
summary  

>> **Page: Chapter**  
chapter  
chapter-number  
focus-questions  
introduction  
learning-objectives  
outline  
outline-remake  

>> **Page: Part**  
part  
part-number  
outline  


>> **Page: TOC**  
ct  
toc   
toc-body  
toc-chapter  
toc-part  
name  

>> **Others**    
restart-numbering  
running-left  
running-right  
start-right  

